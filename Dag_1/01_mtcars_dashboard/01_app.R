library(shiny)

ui <- fluidPage(

  fluidRow(
    column(12,
           tags$h3("Dit is een titel"),
           selectInput("sel_x", "Selecteer Y-as", choices = names(mtcars)),
           selectInput("sel_y", "Selecteer Y-as", choices = names(mtcars))
    )
  ),

  fluidRow(

    column(6,
           plotOutput("plot_1")
    ),
    column(6,
           tableOutput("tab_1")
    )

  )






  # dt_   datatable
  # plot_   plotOutput
  # tab_   tableOutput
  # map_   leaflet
  # ui_    uiOutput (/renderUI)
  # pick_   pickerInput
  # num_    numericInput
  # txt_    textInput
  #

)

server <- function(input, output, session) {


  output$plot_1 <- renderPlot({

    yas <- input$sel_y
    xas <- input$sel_x

    plot(mtcars[[xas]], mtcars[[yas]])

  })


  output$tab_1 <- renderTable({

    yas <- input$sel_y
    xas <- input$sel_x

    mtcars[,c(xas,yas)]

  })



}

shinyApp(ui, server)
