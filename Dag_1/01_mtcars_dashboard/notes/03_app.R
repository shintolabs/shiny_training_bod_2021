
# Zoals app2, maar we gebruiken fluidPage/fluidRow/column om de layout te verbeteren.
# Zie app4 voor toelichting van deze layout methode.


library(shiny)
library(ggplot2)

ui <- fluidPage(
  
  fluidRow(
    column(6, 
           plotOutput("plot_1", height = "400px")
    ),
    column(6, 
           tableOutput("tab_1")
    )
  ),
  fluidRow(
    column(12,
           tags$h4("Filters"),
           selectInput("sel_cyl", "Cylinder", 
                       choices = unique(mtcars$cyl),
                       multiple = TRUE,
                       selected = unique(mtcars$cyl))
    )
  )
  
)

server <- function(input, output, session) {
  
    output$plot_1 <- renderPlot({
      
      plotdata <- filter(mtcars, cyl %in% as.numeric(input$sel_cyl))
      
      ggplot(plotdata, aes(x=wt, y=cyl)) +
        geom_point() +
        theme_minimal(base_size = 14)
      
    })
    
    # Net zoals plotOutput/renderPlot, hebben we voor tables tableOutput/renderTable
    output$tab_1 <- renderTable({
      
      filter(mtcars, cyl %in% as.numeric(input$sel_cyl)) %>%
        group_by(cyl) %>%
        summarize(wt = mean(wt),
                  disp = mean(disp),
                  mpg = mean(mpg))
      
    })
  
}

shinyApp(ui, server)


