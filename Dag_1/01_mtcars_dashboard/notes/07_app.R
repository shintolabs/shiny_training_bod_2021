

# Idem als app5, maar met 2 datatables: selecteer rijen uit de eerste voor rode punten op de plot,
# en rijen uit de 2e voor blauwe punten.

library(shiny)
library(DT)

ui <- fluidPage(
  
  fluidRow(
    column(6, 
           DT::dataTableOutput("dt_mtcars_1"),
           DT::dataTableOutput("dt_mtcars_2")
    ),
    column(6,
           plotOutput("plot_mtcars")       
    )
  ),
  fluidRow(
    column(12,
           
           verbatimTextOutput("txt_out")       
    )
  )
  
  
)

server <- function(input, output, session) {
  
  
  output$dt_mtcars_1 <- DT::renderDataTable({
    
    datatable(mtcars,
              selection = "multiple"
    )
    
  })
  
  output$dt_mtcars_2 <- DT::renderDataTable({
    
    datatable(mtcars,
              selection = "multiple"
    )
    
  })
  
  
  dt_rows_selected_1 <- reactive({
    input$dt_mtcars_1_rows_selected
  })
  
  dt_rows_selected_2 <- reactive({
    input$dt_mtcars_2_rows_selected
  })
  
  
  output$plot_mtcars <- renderPlot({
    
    
    
    p <- ggplot(mtcars, aes(x=wt, y=disp)) +
      geom_point() + 
      theme_minimal(base_size=14)
    
    i_rows_1 <- dt_rows_selected_1()
    if(!is.null(i_rows_1)){
      
      p <- p +
        geom_point(data = mtcars[i_rows_1,],
                   aes(color = "red", size = 3))
      
    }
    
    i_rows_2 <- dt_rows_selected_2()
    if(!is.null(i_rows_2)){
      
      p <- p +
        geom_point(data = mtcars[i_rows_2,],
                   aes(color = "blue", size = 3))
      
    }
    
    p
    
  })
  
  
  output$txt_out <- renderPrint({
    reactiveValuesToList(input)
  })
  
}

shinyApp(ui, server)

