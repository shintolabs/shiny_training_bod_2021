


# Laden dependencies
source("preload/load_packages.R")
source("R/functions.R")
source("R/functions_ui.R")

tictoc::tic("global.R")

# Data inlezen
# --> database connectie aanmaken
mtcars <- mtcars
iris <- iris


# mydataset <- read.csv("data/mydata.csv") %>%
#   mutate() %>%
#   filter()
#

# mydataset <- mutate(...)
# mydataset <- read_and_prepare_mydataset("data/mydata.csv")




# onStop methodes
# onStop({
#   # functies die je wilt aanroepen als de applicatie stopt
#   # connectie naar je database sluiten
# })


tictoc::toc()

