

# als dashboard_02, maar data kunnen sturen NAAR een module.

library(shiny)
library(bslib)
library(ggplot2)
library(ggrepel)
library(shinyjs)
library(DT)
library(dplyr)


mtcars$car <- rownames(mtcars)

# --> documentatie met roxygen2
#' Simple dashboard scatterplot
#' @param xvar Name of the X-variable
#' @param yvar Y-variable
dashboard_scatterplot <- function(xvar, yvar, data, xlab = xvar, ylab=yvar,
                                  label_column = NULL, color = "#ae2012"){
  
  p <- ggplot(data, aes(x= !!sym(xvar), y=!!sym(yvar))) + 
    geom_point(size = 2, color = color) + 
    theme_minimal(base_size = 14) +
    xlab(xlab) + 
    ylab(ylab)
  
  if(!is.null(label_column) && label_column %in% names(data)){
    p <- p + ggrepel::geom_text_repel(aes(label = !!sym(label_column)))
  }
  
  p
  
}

# testthat
# tinytest

test_dashboard_scatterplot <- function(){
  
  dashboard_scatterplot("disp","wt",mtcars,
                        xlab = "Displacement", ylab = "Weight",
                        label_column = "car")
  
}

# --> apart test script, bv. onder test/test_plots.R
#test_dashboard_scatterplot()


filter_mtcars_cyl <- function(cyl){
  
  if(is.null(cyl) || length(cyl) == 0){
    return(mtcars)
  } else {
    mtcars %>%
      filter(cyl %in% !!cyl)    
  }
  
}

# nrow(filter_mtcars_cyl(4))
# nrow(filter_mtcars_cyl(c(4,6)))





#--> Een module (UI + server) zet je normaal in een apart script
# bv. modules/scatterplotModule.R
#
# in global.R:
# source("modules/scatterplotModule.R")
scatterplotModuleUI <- function(id){
  
  ns <- NS(id)
  
  tagList(
    plotOutput(ns("plot_main")),
    selectInput(ns("sel_xvar"), "X-as", choices = names(mtcars)),
    selectInput(ns("sel_yvar"), "Y-as", choices = names(mtcars))
  )
  
}

scatterModule <- function(input, output, session, 
                          data = reactive(NULL),
                          label_column = reactive(NULL)
                          ){
  
  output$plot_main <- renderPlot({
    
    req(data())
    req(nrow(data()) > 2)
    dashboard_scatterplot(xvar = input$sel_xvar,
                          yvar = input$sel_yvar, 
                          data = data(),
                          label_column = label_column()
                          )
  })
  
}






ui <- navbarPage(
  theme = bs_theme(version = 4, bootswatch = "flatly"),
  title = "Shiny modules",
  
  tabPanel(title = "Dashboard",
      
     fluidPage(           
      fluidRow(
        column(4,
               
               scatterplotModuleUI("plot1")    
        ),
        column(4,
               
               scatterplotModuleUI("plot2") 
        ),
        column(4,
               
          tags$h4("Settings"),
          selectInput("sel_cyl", "Cylinders", choices = unique(mtcars$cyl),
                      multiple = TRUE),   
          
          radioButtons("rad_label_data", "Labels", choices = c("Ja" = TRUE, "Nee" = FALSE))
          
        )
      )
     )
  ),
  tabPanel(title = "Data",
           
           fluidPage(           
             DT::dataTableOutput("dt_filtered_data")
           )
  ),
  
  
  
)

server <- function(input, output, session) {
  
  
  mtcars_filtered <- reactive({
    filter_mtcars_cyl(input$sel_cyl)
  })
  
  plot_label_column <- reactive({
    ifelse(input$rad_label_data, "car", "")
  })
  
  
  output$dt_filtered_data <- renderDataTable({
  
    mtcars_filtered() %>%
      datatable()
      
  })
  
  
  callModule(scatterModule, "plot1", data = mtcars_filtered, label_column = plot_label_column)
  callModule(scatterModule, "plot2", data = mtcars_filtered, label_column = plot_label_column)
  
  
}

shinyApp(ui, server)





