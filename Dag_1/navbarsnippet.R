
snippet mynavbar
  library(shiny)
  library(bslib)


  ui <- navbarPage(

    theme = bs_theme(version = 4, bootswatch = "flatly"),
    title = tagList(icon("house-damage"), "App"),

    tabPanel(
      title = tagList(icon("table"), "Tab 1")

    ),
    tabPanel(
      title = tagList(icon("table"), "Tab 2")
    )
  )

  server <- function(input, output, session) {

  }

  shinyApp(ui, server)
