
9:00 - 12:00
  Shiny basis vaardigheden
	- wat is shiny
	- technologieen
	- cursus pagina
	- git
	- rstudio projecten
	- lists, dplyr, glue, NULL, NA, Error/warning/print
	- eerste app
	- source code
	- chrome inspect
	- layout met fluidPage/fluidRow/column
	- CSS intro
	- reactive & reactiveVal

  Oefenen
    - cursus pagina clonen, apps runnen
    - layout met fluidPage
    - stylen met CSS; vinden met chrome inspect
    - app bouwen met reactive en reactiveVal

 12:45 - 16:30
   Code verbeteren
     - functies
     - folder indeling
     - shiny modules (1)
     - testen
     - debuggen (browser, reactiveValuesToList, whereami)

   Oefenen
     - functies schrijven (plot functies; tabel functies)
     - debuggen oefenen
     - app van vanochtend verder bouwen: indeling, tests, functies, readme
     - app debuggen

 Packages
   dplyr,lubridate,glue,ggplot2,whereami,shiny,shinyWidgets,shinyjs,
   bslib,yaml,futile.logger,DT,stringr




oefeningen

reactive/reactiveVal
- voorbeeld app: x en y as selecteren, plot gaat automatisch
  aanpassing: eerst op knop drukken voordat plot update
- 

