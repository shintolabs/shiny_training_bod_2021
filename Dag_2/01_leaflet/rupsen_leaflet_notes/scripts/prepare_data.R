

source("load_packages.R")

buurten <- st_read("data/buurten/buurten.shp") %>%
  st_set_crs(28992) %>%
  st_transform(4326)

rupsen <- st_read("data/meldingen/meldingen-openbare-ruimte.shp") %>%
  st_set_crs(4326)

buurt_rupsen <- st_join(buurten, rupsen)

buurt_rupsen_tab <- count(buurt_rupsen, buurtnaam)



# Export

saveRDS(buurten, "data/processed/buurten.rds")
saveRDS(buurt_rupsen, "data/processed/buurt_rupsen.rds")
saveRDS(rupsen, "data/processed/rupsen.rds")
saveRDS(buurt_rupsen_tab, "data/processed/buurt_rupsen_tab.rds")
