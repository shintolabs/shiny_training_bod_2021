
testModuleUI <- function(id){
  
  ns <- NS(id)
  
  
  uiOutput(ns("test"))
  
  
}


testModule <- function(input, output, session){
  
  # elke module vanuit waar je wilt kunnen klikken krijgt dit blokje code:
  observeEvent(input$click_id, {
    session$userData$click_id(list(id = input$click_id$id, nonce = runif(1)))
  })
  
  
  output$test <- renderUI({
    
    tagList(
      tags$p("Klik op deze auto:"),
      tags$p(HTML(object_link(car_data$car[1], label = car_data$car[1])))
    )
    
    
  })
  
  
}

