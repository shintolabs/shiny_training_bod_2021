
add_draw_toolbar <- function(map){
  
  map <- map %>% 
    leafpm::addPmToolbar(
      drawOptions = pmDrawOptions(snappable = FALSE, 
                                  finishOn = "dblclick"),
      targetGroup = "main_layer",
      toolbarOptions = pmToolbarOptions(
        drawMarker = FALSE,
        drawPolygon = TRUE,
        drawCircle = FALSE,
        drawPolyline = FALSE,
        drawRectangle = FALSE,
        cutPolygon = FALSE,
        editMode = FALSE,
        removalMode = FALSE,
        position = "bottomleft"
      ),
      
      editOptions = pmEditOptions(draggable = FALSE)
    )
  
}

convert_draw_feature <- function(x){
  
  if(is.null(x))return(NULL)
  
  if(x$type == "Feature"){
    
    geom <- x$geometry  
    
    if(geom$type == "Polygon"){
      
      coor <- matrix(unlist(geom$coordinates), ncol = 2, byrow = TRUE)
      
      if(nrow(coor) == 3){
        
        coor <- rbind(coor, coor[3,])
        
      }
      
      st_sfc(st_polygon(list(coor)), crs = 4326)
      
    }
    
    
  } else {
    NULL
  }
  
  
}
